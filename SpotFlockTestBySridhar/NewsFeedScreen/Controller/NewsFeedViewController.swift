//
//  NewsFeedViewController.swift
//  SpotFlockTestBySridhar
//
//  Created by Mohammad Apsar on 08/07/19.
//  Copyright © 2019 Apsar. All rights reserved.
//

import UIKit
import SDWebImage

class NewsFeedViewController: UIViewController {

    @IBOutlet weak var newfeed_tblView: UITableView!
    let viewModel = NewsFeedViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white, NSAttributedString.Key.font:UIFont(name:"HelveticaNeue", size: 26)!]
        self.title = "News Feed"
        self.newfeed_tblView.estimatedRowHeight = 305
        self.newfeed_tblView.rowHeight = UITableView.automaticDimension
        self.addBackButton()
        self.loadNewsFeedService()
    }
    func loadNewsFeedService() {
        SharedClass.sharedInstance.showLoader(message: "Please Wait")
        self.viewModel.OnNewsFeedServiceCallMethod( completionCallBack: { (isSuccess, message) in
            if isSuccess  {
                SharedClass.sharedInstance.hideLoader()
                DispatchQueue.main.async {
                self.newfeed_tblView.reloadData()
                }
            } else {
                SharedClass.sharedInstance.hideLoader()
            }
        })
    }
    @IBAction func onClickActionBtnOnImgView_Act(_ sender: UIButton) {
        DispatchQueue.main.async {
            let story = UIStoryboard.init(name: "Main", bundle: nil)
            let webVC = story.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
            webVC.urlStr = self.viewModel.getArticleUrl(row: sender.tag)
            self.navigationController?.pushViewController(webVC, animated: true)
        }
    }
    func addBackButton() {
        let logoutBtn = UIButton(type: .custom)
        logoutBtn.setTitle("Log Out", for: .normal)
        logoutBtn.setTitleColor(UIColor.white, for: .normal)
        logoutBtn.addTarget(self, action: #selector(self.logOutAction(_:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: logoutBtn)
        self.navigationItem.hidesBackButton = true
    }
    @IBAction func logOutAction(_ sender: UIButton) {
        let _ = self.navigationController?.popViewController(animated: true)
    }
}
extension NewsFeedViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.getNumberOfRows()
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let Cell = tableView.dequeueReusableCell(withIdentifier: "newsfeedCell", for: indexPath) as! NewsFeedTableViewCell
        Cell.titleName_lbl.text = self.viewModel.getTitleName(row: indexPath.row)
        Cell.titleTagLinename_lbl.text = self.viewModel.getTagLineText(row: indexPath.row)
        Cell.fullDescription_lbl.text = self.viewModel.getFullDescriptionText(row: indexPath.row)
        Cell.shortDescription_lbl.text = self.viewModel.getShortDescriptionText(row: indexPath.row)
        Cell.click_btn.tag = indexPath.row
//Cell.titleImg_ImgView.image = self.loadImageFromServer(urlStr: self.viewModel.getImageUrl(row: indexPath.row))
     
      //  Cell.titleImg_ImgView.dowloadFromServer(link: self.viewModel.getImageUrl(row: indexPath.row))
        print(self.viewModel.getImageUrl(row: indexPath.row))
        
        Cell.titleImg_ImgView.sd_setImage(with: URL(string: self.viewModel.getImageUrl(row: indexPath.row)),
                                    placeholderImage: UIImage(named: ""))
//facilities.jpg
        return Cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension

    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        UserDefaults.standard.set(viewModel.getRegionName(atPosition: indexPath.row), forKey: "hotelRegion")
        DispatchQueue.main.async {
            let story = UIStoryboard.init(name: "Main", bundle: nil)
            let webVC = story.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
            webVC.urlStr = self.viewModel.getArticleUrl(row: indexPath.row)
            self.navigationController?.pushViewController(webVC, animated: true)
        }
    }
}
