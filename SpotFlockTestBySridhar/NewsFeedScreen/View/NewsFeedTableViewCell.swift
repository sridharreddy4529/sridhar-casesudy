//
//  NewsFeedTableViewCell.swift
//  SpotFlockTestBySridhar
//
//  Created by Mohammad Apsar on 08/07/19.
//  Copyright © 2019 Apsar. All rights reserved.
//

import UIKit

class NewsFeedTableViewCell: UITableViewCell {

    @IBOutlet weak var click_btn: UIButton!
    @IBOutlet weak var shortDescription_lbl: UILabel!
    @IBOutlet weak var fullDescription_lbl: UILabel!
    @IBOutlet weak var titleImg_ImgView: UIImageView!
    @IBOutlet weak var titleTagLinename_lbl: UILabel!
    @IBOutlet weak var titleName_lbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
