

import Foundation
struct NewsFeedModel : Codable {
	let success : Bool?
	let kstream : Kstream?

	enum CodingKeys: String, CodingKey {
		case success = "success"
		case kstream = "kstream"
	}
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		success = try values.decodeIfPresent(Bool.self, forKey: .success)
		kstream = try values.decodeIfPresent(Kstream.self, forKey: .kstream)
	}
}
