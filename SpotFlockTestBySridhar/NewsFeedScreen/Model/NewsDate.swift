

import Foundation
struct NewsDate : Codable {
	let id : Int?
	let rss_source_id : Int?
	let title : String?
	let title_image : String?
	let tag_line : String?
	let short_description : String?
	let full_description : String?
	let title_image_url : String?
	let description_image_url : String?
	let article_url : String?
	let author : String?
	let article_type : String?
	let published_date : String?
	let is_sponsored : Int?
	let is_premium : Int?
	let tags : String?
	let filtertags : String?
	let likes : Int?
	let comments : Int?
	let shares : Int?
	let meta_kstream_id : Int?
	let accepted : Int?
	let created_at : String?
	let updated_at : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case rss_source_id = "rss_source_id"
		case title = "title"
		case title_image = "title_image"
		case tag_line = "tag_line"
		case short_description = "short_description"
		case full_description = "full_description"
		case title_image_url = "title_image_url"
		case description_image_url = "description_image_url"
		case article_url = "article_url"
		case author = "author"
		case article_type = "article_type"
		case published_date = "published_date"
		case is_sponsored = "is_sponsored"
		case is_premium = "is_premium"
		case tags = "tags"
		case filtertags = "filtertags"
		case likes = "likes"
		case comments = "comments"
		case shares = "shares"
		case meta_kstream_id = "meta_kstream_id"
		case accepted = "accepted"
		case created_at = "created_at"
		case updated_at = "updated_at"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		rss_source_id = try values.decodeIfPresent(Int.self, forKey: .rss_source_id)
		title = try values.decodeIfPresent(String.self, forKey: .title)
		title_image = try values.decodeIfPresent(String.self, forKey: .title_image)
		tag_line = try values.decodeIfPresent(String.self, forKey: .tag_line)
		short_description = try values.decodeIfPresent(String.self, forKey: .short_description)
		full_description = try values.decodeIfPresent(String.self, forKey: .full_description)
		title_image_url = try values.decodeIfPresent(String.self, forKey: .title_image_url)
		description_image_url = try values.decodeIfPresent(String.self, forKey: .description_image_url)
		article_url = try values.decodeIfPresent(String.self, forKey: .article_url)
		author = try values.decodeIfPresent(String.self, forKey: .author)
		article_type = try values.decodeIfPresent(String.self, forKey: .article_type)
		published_date = try values.decodeIfPresent(String.self, forKey: .published_date)
		is_sponsored = try values.decodeIfPresent(Int.self, forKey: .is_sponsored)
		is_premium = try values.decodeIfPresent(Int.self, forKey: .is_premium)
		tags = try values.decodeIfPresent(String.self, forKey: .tags)
		filtertags = try values.decodeIfPresent(String.self, forKey: .filtertags)
		likes = try values.decodeIfPresent(Int.self, forKey: .likes)
		comments = try values.decodeIfPresent(Int.self, forKey: .comments)
		shares = try values.decodeIfPresent(Int.self, forKey: .shares)
		meta_kstream_id = try values.decodeIfPresent(Int.self, forKey: .meta_kstream_id)
		accepted = try values.decodeIfPresent(Int.self, forKey: .accepted)
		created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
		updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
	}

}
