

import Foundation
struct Kstream : Codable {
	let current_page : Int?
	let data : [NewsDate]?
	let first_page_url : String?
	let from : Int?
	let next_page_url : String?
	let path : String?
	let per_page : Int?
	let prev_page_url : String?
	let to : Int?

	enum CodingKeys: String, CodingKey {

		case current_page = "current_page"
		case data = "data"
		case first_page_url = "first_page_url"
		case from = "from"
		case next_page_url = "next_page_url"
		case path = "path"
		case per_page = "per_page"
		case prev_page_url = "prev_page_url"
		case to = "to"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		current_page = try values.decodeIfPresent(Int.self, forKey: .current_page)
		data = try values.decodeIfPresent([NewsDate].self, forKey: .data)
		first_page_url = try values.decodeIfPresent(String.self, forKey: .first_page_url)
		from = try values.decodeIfPresent(Int.self, forKey: .from)
		next_page_url = try values.decodeIfPresent(String.self, forKey: .next_page_url)
		path = try values.decodeIfPresent(String.self, forKey: .path)
		per_page = try values.decodeIfPresent(Int.self, forKey: .per_page)
		prev_page_url = try values.decodeIfPresent(String.self, forKey: .prev_page_url)
		to = try values.decodeIfPresent(Int.self, forKey: .to)
	}

}
