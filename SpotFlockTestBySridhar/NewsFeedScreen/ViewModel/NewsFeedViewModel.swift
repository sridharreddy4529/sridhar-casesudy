//
//  NewsFeedViewModel.swift
//  SpotFlockTestBySridhar
//
//  Created by Mohammad Apsar on 08/07/19.
//  Copyright © 2019 Apsar. All rights reserved.
//

import Foundation
class NewsFeedViewModel {
    var newsFeedData: NewsFeedModel?
    func OnNewsFeedServiceCallMethod(completionCallBack: @escaping (Bool, String?) -> Void) {
        if Reachability.isConnectedToNetwork() {
            let api_token = UserDefaults.standard.object(forKey: "api_token") as! String
            let headerStr = String(format: "Bearer %@", api_token)
            let headers = [
                "accept": "application/json",
                "authorization": headerStr,
            ]
            let url = NSString(format: "%@%@",
                               Constants.APILIST.BASEURL,
                               Constants.APILIST.ktream) as String
            let request = NSMutableURLRequest(url: NSURL(string: url)! as URL,
                                              cachePolicy: .useProtocolCachePolicy,
                                              timeoutInterval: 10.0)
            request.httpMethod = "GET"
            request.allHTTPHeaderFields = headers
            
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                
                if (error != nil) {
                } else {
                    if response != nil {
                        do {
                            let rawData = data
                            let jsonDecoder = JSONDecoder()
                            let newsDetailsModel = try jsonDecoder.decode(NewsFeedModel.self, from: rawData!)
                            self.newsFeedData = newsDetailsModel
                            completionCallBack(true, "True")
                        } catch {
                            print(error)
                            completionCallBack(true, error.localizedDescription)
                        }
                    } else {
                        print("no response")
                        completionCallBack(true, "Something went wrong")
                    }
                }
            })
            dataTask.resume()
        }
    }
    public func getNumberOfRows() -> Int {
        return self.newsFeedData?.kstream?.data?.count ?? 0
    }
    public func getTitleName(row: Int) -> String {
        return self.newsFeedData?.kstream?.data?[row].title ?? ""
    }
    public func getTagLineText(row: Int) -> String {
      return self.newsFeedData?.kstream?.data?[row].tag_line ?? ""
    }
    public func getFullDescriptionText(row: Int) -> String {
        return self.newsFeedData?.kstream?.data?[row].full_description ?? ""
    }
    public func getShortDescriptionText(row: Int) -> String {
        return self.newsFeedData?.kstream?.data?[row].short_description ?? ""
    }
    public func getArticleUrl(row: Int) -> String {
        return self.newsFeedData?.kstream?.data?[row].article_url ?? ""
    }
    public func getImageUrl(row: Int) -> String {
        return self.newsFeedData?.kstream?.data?[row].description_image_url ?? ""
    }
}
