//
//  WebViewController.swift
//  SpotFlockTestBySridhar
//
//  Created by Mohammad Apsar on 09/07/19.
//  Copyright © 2019 Apsar. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController, WKNavigationDelegate {

    @IBOutlet weak var webView: WKWebView!
    var urlStr: String = String()

    override func viewDidLoad() {
        super.viewDidLoad()
       webView.navigationDelegate = self
        // Do any additional setup after loading the view.
        let url = URL(string: urlStr)!
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
