//
//  Constants.swift
//  SpotFlockTestBySridhar
//
//  Created by Mohammad Apsar on 08/07/19.
//  Copyright © 2019 Apsar. All rights reserved.
//
import Foundation

public struct Constants {
    struct APILIST {
        static let BASEURL = "https://gospark.app"
        static let register = "/api/v1/register"
        static let login = "/api/v1/login"
        static let ktream = "/api/v1/kstream"
    }
    struct STRINGCONSTANTS {
        static let ERROR = "Error"
        static let NOINTERNETCONNECTIONTITLE = "No Internet Connection"
    }
}
