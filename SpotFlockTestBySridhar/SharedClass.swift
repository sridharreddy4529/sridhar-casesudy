//
//  SignUpViewModel.swift
//  SpotFlockTestBySridhar
//
//  Created by Mohammad Apsar on 08/07/19.
//  Copyright © 2019 Apsar. All rights reserved.
//
import UIKit
import SVProgressHUD
class SharedClass: NSObject {
    static let sharedInstance = SharedClass()
    let indicator: UIActivityIndicatorView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.white)
    let lblNew = UILabel()
    func loader(view: UIView) {
        indicator.frame = CGRect(x: 0, y: 0, width: 75, height: 75)
        indicator.layer.cornerRadius = 8
        indicator.center = view.center
        //indicator.activityIndicatorViewStyle = .whiteLarge
        view.addSubview(indicator)
        indicator.color = #colorLiteral(red: 0.5900130868, green: 0.5301641822, blue: 0.3781483769, alpha: 1)
        indicator.backgroundColor = #colorLiteral(red: 0.82265625, green: 0.8985491071, blue: 0.9149832589, alpha: 1)
        indicator.bringSubviewToFront(view)
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        indicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
    func dissMissLoader() {
        indicator.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    func showLoader(message: String) {
        SVProgressHUD.show(withStatus: message)
    }
    func hideLoader() {
        SVProgressHUD.dismiss()
    }
    func alertToUser(message: String, inViewCtrl: UIViewController) {
        let alert = UIAlertController(title: "Alert",
                                      message: message,
                                      preferredStyle: UIAlertController.Style.alert)
        let cancelAction = UIAlertAction(title: "Ok",
                                         style: .cancel, handler: nil)
        alert.view.tintColor = #colorLiteral(red: 0.4130901694, green: 0.3841119409, blue: 0.5746166706, alpha: 1)
        alert.addAction(cancelAction)
        inViewCtrl.present(alert, animated: true, completion: nil)
     }
}
extension UIViewController {
    func isValidEmail(emailStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }
   func showToast(message: String) {
          let message = message
          let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
          let messageFont = [NSAttributedString.Key.font: UIFont(name: "Avenir", size: 15.0)!,
                       NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)] as [NSAttributedString.Key: Any]
          let messageAttrString = NSMutableAttributedString(string: message, attributes: messageFont)
          alert.setValue(messageAttrString, forKey: "attributedMessage")
          self.present(alert, animated: true)
          let subview = (alert.view.subviews.first?.subviews.first?.subviews.first!)! as UIView
          subview.layer.cornerRadius = 5
          subview.backgroundColor = #colorLiteral(red: 0.2588235294, green: 0.3450980392, blue: 0.9607843137, alpha: 1)
          subview.tintColor = UIColor.white
          let duration: Double = 2 // duration in seconds
          DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration) {
                alert.dismiss(animated: true)
          }
    }
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
