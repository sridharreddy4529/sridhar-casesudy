

import Foundation
struct loginModel : Codable {
	let status : String?
	let message : String?
	let user : User?

	enum CodingKeys: String, CodingKey {

		case status
		case message
		case user
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decodeIfPresent(String.self, forKey: .status)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		user = try values.decodeIfPresent(User.self, forKey: .user)
	}

}
