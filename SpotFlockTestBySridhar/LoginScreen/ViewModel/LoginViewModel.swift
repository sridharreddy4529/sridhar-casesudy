//
//  LoginViewModel.swift
//  SpotFlockTestBySridhar
//
//  Created by Mohammad Apsar on 08/07/19.
//  Copyright © 2019 Apsar. All rights reserved.
//
import Foundation
class LoginViewModel {
    var loginData: loginModel?
    func OnLoginServiceCallMethod(registartionDict: Dictionary<String, String>, completionCallBack: @escaping (Bool, String?) -> Void) {
        if Reachability.isConnectedToNetwork() {
            let url = NSString(format: "%@%@",
                               Constants.APILIST.BASEURL,
                               Constants.APILIST.login) as String
            APIManager().getTheResponseFromUrl(apiUrl: url, requestType: RequestType.POST, bodyParameters: (registartionDict as NSDictionary) as! Dictionary<String, String>) { (isSuccess, response, error) in
                if isSuccess {
                    if let res = response {
                        do {
                            let rawData = res.data
                            let jsonDecoder = JSONDecoder()
                            let loginDetailsModel = try jsonDecoder.decode(loginModel.self, from: rawData!)
                            self.loginData = loginDetailsModel
                            completionCallBack(true, "True")
                        } catch {
                            print(error)
                            completionCallBack(true, error.localizedDescription)
                        }
                    } else {
                        print("no response")
                        completionCallBack(true, "Something went wrong")
                    }
                } else {
                    print("Registartion not done")
                    SharedClass.sharedInstance.hideLoader()
                    completionCallBack(true, "Something went wrong")
                    
                }
            }
        }
    }
    public func loginSuccessMessage() -> String {
    return self.loginData?.message ?? ""
    }
    public func loginToken() -> String {
        return self.loginData?.user?.api_token ?? ""
    }
    public func checkAuthenticationSuccessOrFail() -> Bool {
        if self.loginData?.status ?? "" == "true" {
            return true
        } else {
            return false
        }
    }
}
