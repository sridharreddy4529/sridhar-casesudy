//
//  LoginViewController.swift
//  SpotFlockTestBySridhar
//
//  Created by Mohammad Apsar on 08/07/19.
//  Copyright © 2019 Apsar. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var password_TF: UITextField!
    @IBOutlet weak var email_TF: UITextField!
    let viewModel = LoginViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLineTexfieldBottomLine()
     //   self.addBackButton()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white, NSAttributedString.Key.font:UIFont(name:"HelveticaNeue", size: 26)!]
        self.title = "Sign In"

        // Do any additional setup after loading the view.
    }
    func addBackButton() {
        let backButton = UIButton(type: .custom)
        backButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        backButton.setImage(UIImage(named: "back"), for: .normal) // Image can be downloaded from here below link
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    @IBAction func backAction(_ sender: UIButton) {
        let _ = self.navigationController?.popViewController(animated: true)
    }
    func setLineTexfieldBottomLine()  {
        self.password_TF.useUnderline()
        self.email_TF.useUnderline()
    }
    @IBAction func onClickSignInBtn_Act(_ sender: Any) {
        self.view.endEditing(true)
        guard let email = self.email_TF.text, email_TF.text?.count ?? 0 > 0 else {
            self.showToast(message: "Please Enter the Email")
            return
        }
        guard isValidEmail(emailStr: email) else {
            self.showToast(message: "Email is not valid")
            return
        }
        guard let password = self.password_TF.text, password_TF.text?.count ?? 0 > 0 else {
            self.showToast(message: "Please Enter the password")
            return
        }
        let params = ["email":email,
                      "password":password]
        self.loginServiceCall(parameters: params)
    }
    func loginServiceCall(parameters: Dictionary<String, String>) {
        SharedClass.sharedInstance.showLoader(message: "Please Wait")
        self.viewModel.OnLoginServiceCallMethod(registartionDict: (parameters as NSDictionary) as! Dictionary<String, String>) { (isSuccess, message) in
            if isSuccess  {
                SharedClass.sharedInstance.hideLoader()
            if self.viewModel.checkAuthenticationSuccessOrFail() {
                
        UserDefaults.standard.set(self.viewModel.loginToken(), forKey: "api_token")
                DispatchQueue.main.async {
                    self.moveToNewFeedScreen()

                    
                }
                
            } else {
              self.showToast(message: self.viewModel.loginSuccessMessage())
                }
            } else {
                print("registration not done")
                SharedClass.sharedInstance.hideLoader()
                self.showToast(message: "Something went wrong")
            }
        }
    }
    func moveToNewFeedScreen() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let newsController = storyBoard.instantiateViewController(withIdentifier: "NewsFeedViewController") as! NewsFeedViewController
        self.navigationController?.pushViewController(newsController, animated: true)
    }
}
extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        return true
    }
}
