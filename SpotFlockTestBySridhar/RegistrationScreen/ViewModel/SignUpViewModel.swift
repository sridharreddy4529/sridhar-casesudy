//
//  SignUpViewModel.swift
//  SpotFlockTestBySridhar
//
//  Created by Mohammad Apsar on 08/07/19.
//  Copyright © 2019 Apsar. All rights reserved.
//

import Foundation
class SignUpViewModel {
    var registrationData: RegistrationModel?
    func OnRegistrationServiceCallMethod(registartionDict: Dictionary<String, String>, completionCallBack: @escaping (Bool, String?) -> Void) {
        if Reachability.isConnectedToNetwork() {
            let url = NSString(format: "%@%@",
                               Constants.APILIST.BASEURL,
                               Constants.APILIST.register) as String
            APIManager().getTheResponseFromUrl(apiUrl: url, requestType: RequestType.POST, bodyParameters: (registartionDict as NSDictionary) as! Dictionary<String, String>) { (isSuccess, response, error) in
                if isSuccess {
                    if let res = response {
                        do {
                            let rawData = res.data
                            let jsonDecoder = JSONDecoder()
                            let registerModel = try jsonDecoder.decode(RegistrationModel.self, from: rawData!)
                            self.registrationData = registerModel
                            completionCallBack(true, "True")
                        } catch {
                            print(error)
                            completionCallBack(true, error.localizedDescription)
                        }
                    } else {
                        print("no response")
                        completionCallBack(true, "Something went wrong")
                    }
                } else {
                print("Registartion not done")
                    SharedClass.sharedInstance.hideLoader()
                    completionCallBack(true, "Something went wrong")

                }
            }
        }
    }
    func isRegistrationDone() -> Bool  {
        if registrationData?.message == "The given data was invalid." {
            return false
        } else {
            return true
        }
    }
    func registrationStatus() -> String {
        if registrationData?.message == "The given data was invalid." {
        //    return registrationData?.errors?.email?[0] ?? "Emails is exist already"
            return "Emails is exist already"
        } else {
            return registrationData?.message ?? "Registration Done"
        }
    }
}
