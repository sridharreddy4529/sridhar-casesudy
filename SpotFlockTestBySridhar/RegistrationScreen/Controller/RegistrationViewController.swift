//
//  RegistrationViewController.swift
//  SpotFlockTestBySridhar
//
//  Created by Mohammad Apsar on 08/07/19.
//  Copyright © 2019 Apsar. All rights reserved.
//

import UIKit

class RegistrationViewController: UIViewController {

    @IBOutlet weak var female_btn: UIButton!
    @IBOutlet weak var male_btn: UIButton!
    @IBOutlet weak var mobileNumber_TF: UITextField!
    @IBOutlet weak var confirmPassword_TF: UITextField!
    @IBOutlet weak var password_TF: UITextField!
    @IBOutlet weak var name_TF: UITextField!
    @IBOutlet weak var email_TF: UITextField!
    var isMaleOrFemale = ""
    let viewModel = SignUpViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       self.setLineTexfieldBottomLine()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white, NSAttributedString.Key.font:UIFont(name:"HelveticaNeue", size: 26)!]
        self.title = "Sign Up"
    
    }
    func setLineTexfieldBottomLine()  {
        self.name_TF.useUnderline()
        self.password_TF.useUnderline()
        self.confirmPassword_TF.useUnderline()
        self.email_TF.useUnderline()
        self.mobileNumber_TF.useUnderline()
    }
    @IBAction func onClickFemaleBnt_Act(_ sender: Any) {
        isMaleOrFemale = "Female"
        self.female_btn.backgroundColor = #colorLiteral(red: 0.2588235294, green: 0.3450980392, blue: 0.9607843137, alpha: 1)
        self.male_btn.backgroundColor = UIColor.lightGray
    }
    @IBAction func onClickSignUp_btn(_ sender: Any) {
        self.view.endEditing(true)

        guard let name = self.name_TF.text, name_TF.text?.count ?? 0 > 0 else {
        self.showToast(message: "Please Enter the Name")
            return
        }
        guard let email = self.email_TF.text, email_TF.text?.count ?? 0 > 0 else {
            self.showToast(message: "Please Enter the Email")
            return
        }
        guard self.isValidEmail(emailStr: email) else {
            self.showToast(message: "Email is not valid")
            return
        }
        guard let password = self.password_TF.text, password_TF.text?.count ?? 0 > 0 else {
            self.showToast(message: "Please Enter the password")
            return
        }
        guard let confirmPassword = self.confirmPassword_TF.text, confirmPassword_TF.text?.count ?? 0 > 0 else {
            self.showToast(message: "Please Enter the confirm Password")
            return
        }
        guard password == confirmPassword else {
            self.showToast(message: "Password and Confirm Password are not matching")
            return
        }
        guard let mobile = self.mobileNumber_TF.text, mobileNumber_TF.text?.count == 10 else {
            self.showToast(message: "Please Enter the 10 digit Mobile number")
            return
        }
        guard isMaleOrFemale != "" else {
            self.showToast(message: "Please select the gender")
            return
        }
        let params = ["name":name,
                      "email":email,
                      "password":password,
                      "password_confirmation":confirmPassword,
                      "mobile":mobile,
                      "gender":isMaleOrFemale]
        self.signUpServiceCall(parameters: params)
    }
    @IBAction func onClieckMale_btn(_ sender: Any) {
        isMaleOrFemale = "Male"
        self.male_btn.backgroundColor = #colorLiteral(red: 0.2588235294, green: 0.3450980392, blue: 0.9607843137, alpha: 1)
        self.female_btn.backgroundColor = UIColor.lightGray
    }
    func signUpServiceCall(parameters: Dictionary<String, String>)  {
       SharedClass.sharedInstance.showLoader(message: "Please Wait")
        self.viewModel.OnRegistrationServiceCallMethod(registartionDict: (parameters as NSDictionary) as! Dictionary<String, String>) { (isSuccess, message) in
            if isSuccess && message != "Something went wrong" {
                SharedClass.sharedInstance.hideLoader()
                DispatchQueue.main.async {
                    self.moveToLoginScreen()
              self.showToast(message: self.viewModel.registrationStatus())
                }
                
            } else {
                print("registration not done")
                SharedClass.sharedInstance.hideLoader()
                self.showToast(message: "Email is already Exist")
            }
    }
}
    func moveToLoginScreen() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let loginController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(loginController, animated: true)
        
    }
}
extension UITextField {
    func useUnderline() {
        let border = CALayer()
        let borderWidth = CGFloat(0.5)
        border.borderColor = UIColor.lightGray.cgColor
        border.frame = CGRect(origin: CGPoint(x: 0,y :self.frame.size.height - borderWidth), size: CGSize(width: self.frame.size.width, height: self.frame.size.height))
        border.borderWidth = borderWidth
        self.borderStyle = UITextField.BorderStyle.none
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}
extension RegistrationViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        return true
    }
}
