

import Foundation
struct RegistrationModel : Codable {
	let success : Bool?
	let message : String?
   let errors : Errors?

	enum CodingKeys: String, CodingKey {
		case success
		case message
       case errors
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        errors = try values.decodeIfPresent(Errors.self, forKey: .errors)
    }
}
