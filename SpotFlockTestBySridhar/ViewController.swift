//
//  ViewController.swift
//  SpotFlockTestBySridhar
//
//  Created by Mohammad Apsar on 08/07/19.
//  Copyright © 2019 Apsar. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBAction func onClickSignInBtn_Act(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let loginController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(loginController, animated: true)
    }
    @IBAction func onClickSIgnUpBtn_ACt(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let registrationController = storyBoard.instantiateViewController(withIdentifier: "RegistrationViewController") as! RegistrationViewController
  self.navigationController?.pushViewController(registrationController, animated: true)
    }
}

