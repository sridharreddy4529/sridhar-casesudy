////
//  SpotFlockTestBySridhar
//
//  Created by Mohammad Apsar on 08/07/19.
//  Copyright © 2019 Apsar. All rights reserved.
//
import Foundation
import UIKit
import Alamofire

class APIManager: NSObject {
    
    func getTheResponseFromUrl(apiUrl: String, requestType: RequestType, bodyParameters: Dictionary<String, String>, completionHandler: @escaping (_ success: Bool,
        _ response: DataResponse<Any>?,
        _ error: String?) -> Void) {
        var urlString = apiUrl
        if urlString.contains(" ") {
            urlString = urlString.replacingOccurrences(of: " ", with: "%20").addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        }
        let url = URL(string: urlString)
        var requestUrl = URLRequest.init(url: url!,
                                         cachePolicy: .reloadIgnoringLocalAndRemoteCacheData,
                                         timeoutInterval: 30)
        requestUrl.httpMethod = requestType.rawValue
        if requestType == RequestType.GET {
            requestUrl.addValue("application/json", forHTTPHeaderField: "Content-Type")
        }
//        if let params = bodyParameters {
//            requestUrl.httpBody = try? JSONSerialization.data(withJSONObject: params)
//        }
    Alamofire.request(urlString, method: .post, parameters: bodyParameters, encoding: JSONEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .responseJSON { response in
                debugPrint(response)
                if response.result.isSuccess {
                    completionHandler(true, response, nil)
                } else {
                    completionHandler(false, nil, response.result.error?.localizedDescription)
                }
        }
    }

    
}
