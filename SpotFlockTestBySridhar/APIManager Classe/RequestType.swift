//
//  SpotFlockTestBySridhar
//
//  Created by Mohammad Apsar on 08/07/19.
//  Copyright © 2019 Apsar. All rights reserved.
//
import Foundation

enum RequestType: String {
    case POST = "post"
    case PUT = "put"
    case GET = "get"
    case DELETE = "delete"
}
